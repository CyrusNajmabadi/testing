#!/bin/bash
# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x
export PATH=$PATH:$HOME/.pulumi/bin
cd teststack
yarn install
pulumi stack select teststack-dev
# The following is just a sample config setting that the hypothetical pulumi
# program needs.
# Learn more about pulumi configuration at: https://pulumi.io/reference/config.html
pulumi config set mysetting:myvalue
pulumi up --color=always
